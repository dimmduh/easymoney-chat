(function(global) {

    "use strict;"

    var Config = {};

    Config.apiBaseUrl = "http://192.168.0.107:666/spika/v1";
    Config.socketUrl = "http://192.168.0.107:666/spika";
    
    Config.googleMapAPIKey = "";
    
    Config.defaultContainer = "#spika-container";
    Config.lang = "ru";
    Config.showSidebar = true;
    Config.showTitlebar = true;
    Config.useBothSide = false;
    Config.thumbnailHeight = 256;
    
    // Exports ----------------------------------------------
    module["exports"] = Config;

})((this || 0).self || global);
