(function(global) {
    "use strict;"

    // Class ------------------------------------------------
    var Const = {};

    Const.httpCodeSucceed = 200;
    Const.httpCodeFileNotFound = 404;
    Const.httpCodeForbidden = 403;
    Const.httpCodeSeverError = 500;
    Const.httpCodeAuthError = 503;
    
    Const.responsecodeSucceed = 1;
    Const.resCodeLoginNoName = 1000001;
    Const.resCodeLoginNoRoomID = 1000002;
    Const.resCodeLoginNoUserID = 1000003;
    Const.resCodeUserListNoRoomID = 1000004;
    Const.resCodeMessageListNoRoomID = 1000005;
    Const.resCodeMessageListNoLastMessageID = 1000006;
    Const.resCodeSendMessageNoFile = 1000007;
    Const.resCodeSendMessageNoRoomID = 1000008;
    Const.resCodeSendMessageNoUserID = 1000009;
    Const.resCodeSendMessageNoType = 1000010;
    Const.resCodeFileUploadNoFile = 1000011;
    
    Const.resCodeSocketUnknownError = 1000012;
    Const.resCodeSocketDeleteMessageNoUserID = 1000013;
    Const.resCodeSocketDeleteMessageNoMessageID = 1000014;
    Const.resCodeSocketSendMessageNoRoomID = 1000015;
    Const.resCodeSocketSendMessageNoUserId = 1000016;
    Const.resCodeSocketSendMessageNoType = 1000017;
    Const.resCodeSocketSendMessageNoMessage = 1000018;
    Const.resCodeSocketSendMessageNoLocation = 1000019;
    Const.resCodeSocketSendMessageFail = 1000020;
    Const.resCodeSocketUserBanned = 1000030;

    Const.resCodeSocketTypingNoUserID = 1000021;
    Const.resCodeSocketTypingNoRoomID = 1000022;
    Const.resCodeSocketTypingNoType = 1000023;
    Const.resCodeSocketTypingFaild = 1000024;
    
    Const.resCodeSocketLoginNoUserID = 1000025;
    Const.resCodeSocketLoginNoRoomID = 1000026;
    Const.resCodeSocketNotEnoughMoney = 1000032;
    
    Const.resCodeTokenError = 1000027;

    Const.resCodeStickerListFailed = 1000028;

    Const.resCodeUserBanned = 1000029;

    Const.resCodeOldVersion = 1000031;
    
    Const.responsecodeParamError = 2001;
    Const.responsecodeTokenError = 2100;

    Const.messageTypeText = 1;
    Const.messageTypeFile = 2;
    Const.messageTypeLocation = 3;
    Const.messageTypeContact = 4;
    Const.messageTypeSticker = 5;
    
    Const.messageNewUser = 1000;
    Const.messageUserLeave = 1001;

    Const.typingOn = 1;
    Const.typingOff = 0;
    
    Const.pagingLimit = 50;

    Const.version = 59;

    Const.notificationSendMessage = "SendMessage";
    Const.notificationNewUser = "NewUser";
    Const.notificationUserBan = "UserBan";
    Const.notificationUserTyping = "UserTyping";
    Const.notificationMessageChanges = "MessageChanges";

    Const.admins = [
        '9a2ac8a12de01cd0b116525e795d4c15',
        'fe167b1e1b74129c5befba887cadd1dc',
        '28b51c838a24a2114745784ac6f861c3',
        'da369382b6077815624ad51a588412c0',
        '8633db165de34ca1138a1a99be923e4c',
        '68cc58d516c3e4b492ee031bb51f9bf9',
        'a4b69ee3bbd3744c7fbe30e0b83b200f',
        '025e81e72b6b4241011ca2045b312eb6',
        'b1d6558cac4589ab7dd37c745984705c',
        'b98c6e690385fd8ee057f6daf6571d9b',
        'f06f3b0806921845360b847b21f42c56',
        'd852b524c2bc38ead37ac6dd14289aed',
        'f864ff8289364be9d4fb16cd80544d3c'
    ];

    Const.adminBadge = 1;
    Const.developerBadge = 2;

    Const.adminWarningMessage = "Do not provide data related to your payment systems to other users. This information can be used by intruders and steal your money. If you are confronted with suspicious users, then immediately report them to the administration through application support.";

    // Exports ----------------------------------------------
    module["exports"] = Const;

})((this || 0).self || global);
