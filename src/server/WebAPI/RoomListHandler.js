var express = require('express');
var router = express.Router();
var _ = require('lodash');

var RequestHandlerBase = require("./RequestHandlerBase");
var Utils = require("../lib/Utils");
var RoomModel = require("../Models/RoomModel");
const UserModel = require('../Models/UserModel');
var Const = require("../const");

var RoomListHandler = function(){
	
}

const admins = [
	'912d4c7c69f496c634c8fabe539bf942',
	'9a2ac8a12de01cd0b116525e795d4c15',
	'754d8c362f1103310660a2318684e1bc',
	'00de7ee521e54f4001344d06e4eaf34f',
	'fe167b1e1b74129c5befba887cadd1dc',
	'eead0386f20faa7b6af2b2406b6be608',
	'a2e382fb617e0bc60242bddafceca488',
	'e2ec1f4e868c55b545703a836b3ad852',
	'de3761d611fd7502f861531ee83dd60b',
	'b78ef8e7012862eeb352a7cf64ef07c9',
];

_.extend(RoomListHandler.prototype,RequestHandlerBase.prototype);

RoomListHandler.prototype.attach = function(router){
		
	var self = this;

	router.get('/',async function(request,response) {
		let roomList = [];
		let appID = await UserModel.getAppID(request.query.userID);
		let language = request.headers['a4o-language'];
		if (admins.indexOf(request.query.userID) > -1) {
			roomList = await RoomModel.findRoomsByApp(appID);
		} else {
			roomList = await RoomModel.findRoomsByAppAndLanguage(appID, language);
		}
		
		self.successResponse(response, Const.responsecodeSucceed, roomList);
	});

}

new RoomListHandler().attach(router);
module["exports"] = router;