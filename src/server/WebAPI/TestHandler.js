var express = require('express');
var router = express.Router();
var _ = require('lodash');

var RequestHandlerBase = require("./RequestHandlerBase");

var TestHandler = function(){
    
}

_.extend(TestHandler.prototype,RequestHandlerBase.prototype);

TestHandler.prototype.attach = function(route){
    var self = this;

    route.get('/',function(request,response){
        response.send('Hello');
    });

}

new TestHandler().attach(router);
module["exports"] = router;
