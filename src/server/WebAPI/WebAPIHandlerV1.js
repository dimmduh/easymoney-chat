var express = require('express');
var router = express.Router();

var bodyParser = require("body-parser");
var Settings = require("../lib/Settings");

var Settings = require("../lib/Settings");

var WebAPIHandler ={
    
    init: function(app,express){
                
        var self = this;
        
        app.use(Settings.options.urlPrefix,express.static(__dirname + '/../../../public'));
        app.use(bodyParser.json());

        app.use((req, res, next) => {
            res.set('Content-Type', 'application/json');
            res.set('Access-Control-Allow-Origin', '*');
            res.set('Access-Control-Allow-Headers', 'Accept, X-Requested-With, Authorization, Content-Type');
            res.set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH');
            res.set('Access-Control-Allow-Credentials', true);
            next();
        });
        
        // HTTP Routes
        router.use("/user/login", require('./LoginHandler'));
        router.use("/temp", require('./TempHandler'));
        router.use("/message/list", require('./MessageListHandler'));
        router.use("/message/latest", require('./LatestMessageListHandler').router);
        router.use("/user/list", require('./UserListHandler'));
        router.use("/message/sendFile", require('./SendFileAsMessageHandler'));
        router.use("/file/upload", require('./FileUploadHandler'));
        router.use("/file/download", require('./FileDownloadHandler'));
        router.use("/test", require('./TestHandler'));
        router.use("/stickers", require('./StickerListHandler'));
        router.use("/rooms", require('./RoomListHandler'));
        
        WebAPIHandler.router = router;
        app.use(Settings.options.urlPrefix + "/v1", router);
        
    }
}

module["exports"] = WebAPIHandler;