const mongoose = require('mongoose');
const _ = require('lodash');
const Const = require('../const.js');
const async = require('async');
const Util = require('../lib/Utils');
const Settings = require("../lib/Settings");
const Promise = require("bluebird");
const UserModel = require('./UserModel');
// const RoomModel = require('./RoomModel');

let MessageModel = function(){
	
};

MessageModel.prototype.model = null;

MessageModel.prototype.init = function(){

	// Defining a schema
	let messageSchema = new mongoose.Schema({
		user: { type: mongoose.Schema.Types.ObjectId, index: true },
		localID: { type: String, index: true },
		userID: { type: String, index: true },
		roomID: { type: String, index: true },
		type: Number,
		message: String,
		image: String,
		file: {
			file: {
				id: mongoose.Schema.Types.ObjectId,
				name: String,
				size: Number,
				mimeType: String
			},
			thumb: {
				id: mongoose.Schema.Types.ObjectId,
				name: String,
				size: Number,
				mimeType: String
			}
		},
		location: {
			lat: Number,
			lng: Number
		},
		replyTo: String,
		replyToMessageID: String,
		deleted: Number,
		created: Number,
		botAnswer: String,
		viewed: Boolean,
		appID: Number,
		complaints: [],
		attributes: {}
	});
	
	this.model = mongoose.model(Settings.options.dbCollectionPrefix + "messages", messageSchema);
	return this.model;
}

MessageModel.prototype.findMessagebyId = async function(id, callback){
	try {
		let model = this.model.findOne({ _id: id });
		if (callback) {
			callback(null, model);
		}
		return model;
	} catch (err) {
		console.error(err);
	}
}

MessageModel.prototype.findAllMessages = function(roomID,lastMessageID,callBack){
	var self = this;
	this.model.findOne({ _id: lastMessageID }, function (err, message) {
		if (err) {
			callBack(err, null);
		}
		
		var query = {
			roomID:roomID
		};
		
		if (message) {
			var lastCreated = message.created;
			query.created = {$gt:lastCreated};
		}
		
		var query = self.model.find(query).sort({'created': 'desc'});
		
		query.exec(function(err, data){
			if (err) {
				console.error(err);
			}
			
			if (callBack) {
				callBack(err, data);
			}
		});
	});

}

// TODO REWRITE THIS SHIT
MessageModel.prototype.findMessages = function(roomID,lastMessageID,limit,callBack){ 
	if (lastMessageID != 0){
		let self = this;
		
		this.model.findOne({ _id: lastMessageID},function (err, message) {
			if (err) {
				return console.error(err);
			}
			
			let query = self.model.find({
				roomID: roomID,
				created: {$lt: message.created},
				deleted: {$exists: false},
			}).sort({'created': 'desc'}).limit(limit);
			
			query.exec(function(err,data){
				if (err) {
					console.error(err);
				}

				if (callBack) {
					callBack(err,data);
				}
			});
		});
		
	} else {
		let query = this.model.find({roomID:roomID, deleted:{$exists: false}}).sort({'created': 'desc'}).limit(limit);        
	
		query.exec(function(err,data){
			if (err) {
				return console.error(err);
			}
			
			if (callBack) {
				callBack(err,data);
			}
		});
	}
}

MessageModel.prototype.populateMessages = async function(messages, userID, callback) {
	if (!_.isArray(messages)) {
		messages = [messages];
	}
	
	// collect ids
	let ids = [];

	messages.forEach(function(row) {
		ids.push(row.userID);

		if (!Util.isEmpty(row.replyTo)) {
			ids.push(row.replyTo);
		}
	});

	if (ids.length == 0) {
		if (callback) {
			callback(null, messages);
		}
	}

	let userResult = await UserModel.findUsersbyInternalId(ids);
	let resultAry = [];
	
	await Promise.map(messages, async function(messageElement) {
		let obj = messageElement.toObject();

		_.forEach(userResult, function(userElement) {
			if (messageElement.userID == userElement.userID) {
				obj.user = userElement.toObject();
			}
		});

		if (!Util.isEmpty(obj.replyTo)) {
			let reply = false;
			_.forEach(userResult, function(userElement) {
				if (messageElement.replyTo.toString() == userElement._id.toString()) {
					reply = true;
					obj.replyTo = {
						id: userElement.userID,
						name: userElement.name,
						avatarURL: userElement.avatarURL
					};
				}
			});
			if (!reply) {
				obj.replyTo = null;
			}
		}

		let complaints = messageElement.complaints;

		if (_.indexOf(complaints, userID) == -1 || messageElement.userID == userID) {
			resultAry.push(obj);
		}

		complaints = null;
		obj = null;
	});

	userResult = null;
	ids = null;

	if (callback) {
		callback(null,resultAry);
	} else {
		return resultAry;
	}
}

MessageModel.prototype.deleteByUserID = async function(userID, roomID) {
	await this.model.where({
		userID: userID,
		roomID: roomID
	}).setOptions({multi: true}).update({deleted: Util.now(), location: null, file: null}).exec();
}

MessageModel.prototype.deleteByUserIDInLastHour = async function(userID, roomID) {
	await this.model.where({
		roomID: roomID,
		created: {
			'$gte': Util.now() - 3600 * 1000
		},
		userID: userID
	}).setOptions({multi: true}).update({deleted: Util.now(), location: null, file: null}).exec();
}

MessageModel.prototype.findMessagesInLastHour = async function(userID, roomID) {
	let messages = await this.model.find({
		roomID: roomID,
		userID: userID,
		created: {
			'$gte': Util.now() - 3600 * 1000
		}
	}).exec();
	return messages;
}

MessageModel.prototype.findMessagesByUserID = async function(userID, roomID) {
	let messages = await this.model.find({
		roomID: roomID,
		userID: userID,
	}).exec();
	return messages;
}

MessageModel.prototype.setViewed = async function(userID, roomID) {
	await this.model.where({roomID: roomID, userID: {$ne: userID}}).setOptions({multi: true}).update({viewed: true}).exec();
}

module["exports"] = new MessageModel();