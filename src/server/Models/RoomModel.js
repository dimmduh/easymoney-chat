const mongoose = require('mongoose');
const _ = require('lodash');
const Const = require('../const.js');
const async = require('async');
const Utils = require('../lib/Utils');
const Settings = require("../lib/Settings");
const Promise = require("bluebird");
const MessageModel = require('./MessageModel');
const UserModel = require('./UserModel');

var RoomModel = function(){
	
};

RoomModel.prototype.model = null;

const getAppID = async function(userID) {
	let user = UserModel.findUserbyId(userID);
	if ((user === null) || Utils.isEmpty(user.appID)) {
		return await App4OfferService.getAppID(userID);
	} else {
		return user.appID;
	}
}

RoomModel.prototype.init = function(){

	// Defining a schema
	var roomSchema = new mongoose.Schema({
		roomID: { type: String, index: true },
		appID: Number,
		icon: String,
		rank: Number,
		cost: Number,
		language: String
	});

	roomSchema.method.isPaid =  function() {
		return roomData[0].cost > 0;
	}
	
	this.model = mongoose.model(Settings.options.dbCollectionPrefix + "rooms", roomSchema);
	return this.model;
}

RoomModel.prototype.findRoomsByApp = async function(appID) {
	let rooms = await this.model.find({appID: appID}).sort({rank: 'asc'}).exec();
	let roomData = [];
	return await Promise.map(rooms, async function(room) {
		roomData = await MessageModel.model.findOne({roomID: room.roomID}).exists('deleted', false).sort({'created': 'desc'}).exec();
		return {
			dialogName: room.roomID,
			icon: room.icon,
			text: (roomData !== null) ? roomData.message : '',
		}
	})
}

RoomModel.prototype.findRoomsByAppAndLanguage = async function(appID, language) {
	let rooms = await this.model.find({
		appID: appID,
		language: language
	}).sort({rank: 'asc'}).exec();
	let roomData = [];
	return await Promise.map(rooms, async function(room) {
		roomData = await MessageModel.model.findOne({
			roomID: room.roomID,
		}).exists('deleted', false).sort({'created': 'desc'}).exec();
		return {
			dialogName: room.roomID,
			icon: room.icon,
			text: (roomData !== null) ? roomData.message : '',
		}
	})
}

RoomModel.prototype.findRoomsByLanguage = async function(language) {
	let rooms = await this.model.find({language: language}).sort({rank: 'asc'}).exec();
	let roomData = [];
	return await Promise.map(rooms, async function(room) {
		roomData = await MessageModel.model.findOne({roomID: room.roomID}).exists('deleted', false).sort({'created': 'desc'}).exec();
		return {
			dialogName: room.roomID,
			icon: room.icon,
			text: (roomData !== null) ? roomData.message : '',
		}
	})
}

RoomModel.prototype.findRoomByID = async function(roomID) {
	try {
		let room = await this.model.findOne({roomID: roomID}).exec();
		return room;
	} catch (err) {
		return null;
	}
}

RoomModel.prototype.needToMakeAdminWarningMessage = async function(roomID) {
	let room = await this.model.findOne({roomID: roomID}).exec();
	if (room === null) {
		return false;
	}

	if (roomID.search('support') >= 0) {
		return false;
	}

	let issetMessages = await MessageModel.model.count({roomID: roomID}).exec();
	return !issetMessages;
}

RoomModel.prototype.isPaidRoom = async function(roomID) { 
	const roomData = await this.model.find({roomID: roomID}).exec(); 
	if (!_.isEmpty(roomData)) { 
		return roomData[0].cost > 0; 
	} else { 
		return false; 
	} 
}

RoomModel.prototype.findRoom = function() {

}

module["exports"] = new RoomModel();