var mongoose = require('mongoose');
var _ = require('lodash');
var Const = require('../const.js');
var async = require('async');
var Utils = require('../lib/Utils');
var Settings = require("../lib/Settings");
const PushNotifiactionService = require('../lib/PushNotificationService');
const App4OfferService = require('../lib/App4OfferService');

const banPeriod = {
	1: 'for 1 day',
	2: 'for 3 days',
	3: 'for a week',
	4: 'forever'
};

var UserModel = function(){
	
};

UserModel.prototype.model = null;

UserModel.prototype.init = function(){
	// Defining a schema
	var userSchema = new mongoose.Schema({
		userID: {type: String, index: true},
		appID: Number,
		name: String,
		avatarURL: String,
		token: String,
		registrationToken: String,
		created: Number,
		bannedTill: Number,
		banCount: Number,
		banned: Boolean,
		badge: Number
	});

	userSchema.methods.ban = async function() {
		if (!Utils.isEmpty(this.bannedTill) && ((this.bannedTill > Utils.now()) || (this.bannedTill == 0))) {
			return;
		}

		let banCount = 1;
		if (!Utils.isEmpty(this.banCount)) {
			banCount = this.banCount + 1;
		}
		let bannedTill = Utils.now();
		
		switch (banCount) {
			case 1 : bannedTill += 3600 * 24 * 1000;
				break;
			case 2 : bannedTill += 3600 * 24 * 3 * 1000;
				break;
			case 3 : bannedTill += 3600 * 24 * 7 * 1000;
				break;
			default : bannedTill = 0;
				break;
		}
		this.update({banCount: banCount, bannedTill: bannedTill, banned: true}).exec();
		PushNotifiactionService.ban(this, banPeriod[banCount]);
	};
 
	this.model = mongoose.model(Settings.options.dbCollectionPrefix + "users", userSchema);
	return this.model;
}

UserModel.prototype.findUserbyId = async function(id,callback){
	try {
		let user = await this.model.findOne({userID: new RegExp("^" + id + "$","g")});
		if (callback) {
			callback(null, user);
		} else {
			return user;
		}
	} catch (err) {
		console.error(err);
	}
}

UserModel.prototype.findUsersbyInternalId = async function(aryId, callback){
	let conditions = [];
	aryId.forEach(function(userId){
		conditions.push({
			userID : userId
		});
	});
	
	let data = [];
	try {
		data = await this.model.find({$or: conditions}).sort({'created': 1}).exec();
	} catch (err) {}
	
	if (callback) {
		callback(null, data)
	} else {
		return data;
	}
}

UserModel.prototype.banUserById = async function(userID) {
	let isAdmin = App4OfferService.getUserIsAdmin(userID);
	if (!isAdmin) {
		let user = await this.findUserbyId(userID);
		user.ban();
	}
}

UserModel.prototype.getAppID = async function(userID) {
	let user = await this.findUserbyId(userID);
	if ((user === null) || Utils.isEmpty(user.appID)) {
		return await App4OfferService.getAppID(userID);
	} else {
		return user.appID;
	}
}

module["exports"] = new UserModel();