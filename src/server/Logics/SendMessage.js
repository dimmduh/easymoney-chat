var _ = require('lodash');

var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");
var UserModel = require("../Models/UserModel");
var MessageModel = require("../Models/MessageModel");
var RoomModel = require('../Models/RoomModel');
var Settings = require("../lib/Settings");
var Observer = require("node-observer");
const app4OfferService = require('../lib/App4OfferService');
const BotService = require('../lib/BotService');

var SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');

const validateMessage = async function(message, roomID, user) {
	let text = message.replace(/\n|\r/g, '');
	let link = null;
	let promo = null;
	let isPaidRoom = await RoomModel.isPaidRoom(roomID);
	if (!isPaidRoom) {
		link = text.match(/(?:\b[a-z\d.-]+:\/\/[^<>\s]+|\b(?:(?:(?:[^\s!@#$%^&*()_=+[\]{}\|;:'",.<>\/?]+)\.)+(?:ru|com|cc|org|me|gg|us|de|kz|net|by|io|info|biz|ua|uz|gl|co|az)|(?:(?:[0-9]|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])\.){3}(?:[0-9]|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5]))(?:[;\/][^#?<>\s]*)?(?:\?[^#<>\s]*)?(?:#[^<>\s]*)?(?!\w))/i);
		// promo =  text.match(/[a-zA-Z0-9АВЕЗКМНОРСТХаекорсх]{1}[.,;:! -=+|\n]*[a-zA-Z0-9АВЕЗКМНОРСТХаекорсх]{1}[.,;:! -=+|\n]*[a-zA-Z0-9АВЕЗКМНОРСТХаекорсх]{1}[.,;:! -=+|\n]*[a-zA-Z0-9АВЕЗКМНОРСТХаекорсх]{1}[.,;:! -=+|\n]*[a-zA-Z0-9АВЕЗКМНОРСТХаекорсх]{1}[.,;:! -=+|\n]*[a-zA-Z0-9АВЕЗКМНОРСТХаекорсх]{1}[.,;:! -=+|\n]*/);
	}

	if ((link !== null) || (promo !== null)) {
		await user.update({banned: true});
		return false;
	}

	return text;
}

const getFile = function(file) {
	let result = {
		file: {
			id: file.file.id,
			name: file.file.name,
			size: file.file.size,
			mimeType: file.file.mimeType
		}
	};
	if(!Utils.isEmpty(file.thumb)){
		result.file.thumb = {
			id: file.thumb.id,
			name: file.thumb.name,
			size: file.thumb.size,
			mimeType: file.thumb.mimeType
		};
	}
	return result;
};

const checkUserBan = function(user, direct, onError) {
	if (!Utils.isEmpty(user.bannedTill) && !direct) {
		if (user.bannedTill == 0) {
			onError('User banned', Const.resCodeSocketUserBanned, {till: -1});
			return true;
		}

		if (user.bannedTill > Utils.now()) {
			onError('User banned', Const.resCodeSocketUserBanned, {till: user.bannedTill - Utils.now()});
			return true;
		}
	}
	return false;
}

const SendMessage = {

	execute: async function(userID, param, onSucess, onError) {
		
		let user = await UserModel.findUserbyId(userID);
		let text = await validateMessage(param.message, param.roomID, user);

		if (checkUserBan(user, param.direct, onError)) {
			return;
		}

		if (text === false) {
			onError('User banned', {till: -1});
			return;
		}

		text = text.replace('\n', ' ');

		if ((text == '') && Utils.isEmpty(param.file)) {
			onSucess();
			return;
		}

		let replyTo = null;
		let replyToMessage = null;
		if (!Utils.isEmpty(param.messageID)) {
			let replyMessage = await MessageModel.findMessagebyId(param.messageID);
			replyTo = replyMessage.user;
			replyToMessage = replyMessage._id.toString();
		}

		let objMessage = {
			user: user._id,
			userID: userID,
			roomID: param.roomID,
			message: text,
			localID: param.localID,
			type: param.type,
			file: null,
			appID: !Utils.isEmpty(user.appID) ? user.appID : 2,
			replyTo: replyTo,
			replyToMessageID: replyToMessage,
			attributes: param.attributes,
			created: Utils.now(),
			botAnswer: null,
			viewed: (!param.direct || UsersManager.getUsers(param.roomID).length >= 2)
		};

		if (!Utils.isEmpty(param.file)) {
			objMessage.file = getFile(param.file);
		}

		// save to database
		let room = await RoomModel.findRoomByID(objMessage.roomID);
		let newMessage = new DatabaseManager.messageModel(objMessage);
		let paid = await app4OfferService.payForMessage(objMessage.userID, (Utils.isEmpty(room) ? 0 : room.cost));

		if (!paid) {
			onError('Not enough money');
			return;
		}

		if (newMessage.roomID.match(new RegExp('support', 'gi')) !== null) {
			newMessage.botAnswer = BotService.getAnswer(newMessage.message);
		}

		try {
			let message = await newMessage.save();
			let data = await MessageModel.populateMessages(message, userID);
			let messageObj = data[0];
			messageObj.canComplain = true;

			if (typeof message.delete == 'undefined') {
				SocketAPIHandler.io.of(Settings.options.socketNameSpace).in(param.roomID).emit('newMessage', messageObj);
				Observer.send(this, Const.notificationSendMessage, messageObj);

				if(onSucess) {
					onSucess(message);
				}
			}
		} catch (err) {
			if (onError) {
				onError(err);
			}
		}
	}
}

module["exports"] = SendMessage;