const express = require('express');
const router = express.Router();
const async = require('async');
const formidable = require('formidable');
const fs = require('fs-extra');
const path = require('path');
const mime = require('mime');
const bodyParser = require("body-parser");
const _ = require('lodash');

const UsersManager = require("../lib/UsersManager");
const DatabaseManager = require("../lib/DatabaseManager");
const Utils = require("../lib/Utils");
const SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');
const UserModel = require("../Models/UserModel");
const RoomModel = require("../Models/RoomModel");
const Settings = require("../lib/Settings");
const Const = require("../const");
const App4OfferService = require('../lib/App4OfferService');

let i18n = require('i18n');

i18n.configure({
    'locales': ['ru-RU', 'en-US'],
    'directory': __dirname + '/../locales'
});

const LoginLogic = {

	checkParams : function(param, onError) {
		if (Utils.isEmpty(param.version) || (param.version < Const.version)) {
			if (onError) {
				onError(null, Const.httpCodeAuthError);
				// onError(null, Const.resCodeOldVersion);
			}
			return false;
		}
		
		if (Utils.isEmpty(param.name)) {
			if (onError) {
				onError(null, Const.httpCodeAuthError);
				// onError(null, Const.resCodeLoginNoName);
			}
			return false;
		}

		if (Utils.isEmpty(param.avatarURL)) {
			param.avatarURL = Settings.options.noavatarImg;
		}

		if (Utils.isEmpty(param.roomID)) {
			if (onError) {
				onError(null, Const.httpCodeAuthError);
				// onError(null, Const.resCodeLoginNoRoomID);
			}
			return false;
		}
		
		if (Utils.isEmpty(param.userID)) {
			if (onError) {
				onError(null, Const.httpCodeAuthError);
				// onError(null, Const.resCodeLoginNoUserID);
			}
			return false;
		}
		return true;
	},

	addNewUser : async function(param, token) {
		let appID = await App4OfferService.getAppID(param.userID);
		let user = DatabaseManager.userModel({
			userID: param.userID,
			appID: appID,
			name: param.name,
			avatarURL: param.avatarURL,
			token: token,
			registrationToken: await App4OfferService.getRegistrationToken(param.userID),
			created: Utils.now(),
			banCount: 0,
			banned: false,
			badge: param.badge,
		});
		return await user.save();
	},

	execute : async function(param, onSuccess, onError) {
		if (!this.checkParams(param, onError)) {
			return;
		};
		
		let token = Utils.randomString(24);
		let user = await UserModel.findUserbyId(param.userID);

		if (Utils.isEmpty(param.badge)) {
			param.badge = 0;
		}
		
		try {
			if (user === null) {
				user = await this.addNewUser(param, token);
			} else {
				let registrationToken = await App4OfferService.getRegistrationToken(param.userID);
				let data = {
					name: param.name,
					avatarURL: param.avatarURL,
					token: token,
					registrationToken: registrationToken,
					badge: param.badge
				};
				if (Utils.isEmpty(user.appID)) {
					data.appID = await App4OfferService.getAppID(user.userID);
				}
				await user.update(data).exec();
			}
		} catch (err) {
			if (onError) {
				onError(err, null);
			}
		}

		user = await UserModel.findUserbyId(param.userID);
		
		let room = await RoomModel.findRoomByID(param.roomID);
		let canPostPhoto = (room !== null) ? await App4OfferService.canPostPhoto(user.userID, param.roomID, (room.cost > 0)) : true;
		let isAdmin = (room !== null) ? await App4OfferService.getUserIsAdmin(user.userID) : false;

		if (await RoomModel.needToMakeAdminWarningMessage(param.roomID)) {
			let adminUser = await UserModel.model.findOne({userID: 'admin'}).exec();
			let localeData = await App4OfferService.getLocale(param.userID);
			i18n.setLocale(localeData.locale);
			let message = new DatabaseManager.messageModel({
				user: adminUser._id,
				userID: adminUser.userID,
				roomID: param.roomID,
				message: i18n.__mf(Const.adminWarningMessage),
				type: 1,
				file: null,
				appID: !Utils.isEmpty(user.appID) ? user.appID : 2,
				created: Utils.now() - 50,
				viewed: true
			});
			await message.save();
		}

		if ((room !== null) && (user.appID != room.appID)) {
			if (onError) {
				onError(new Error("AppId of room not equal user's appID"), Const.httpCodeForbidden);
			}
			return;
		}

		if (onSuccess) {
			onSuccess({
				token: token,
				user: user,
				canPostPhoto: canPostPhoto,
				isAdmin: isAdmin,
				cost: (room !== null) ? room.cost : 0
			});
		}
	}
}

module["exports"] = LoginLogic;