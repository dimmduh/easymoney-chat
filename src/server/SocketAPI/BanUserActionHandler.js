var _ = require('lodash');
var Observer = require("node-observer");

var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");
var SocketHandlerBase = require("./SocketHandlerBase");
var SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');
var MessageModel = require("../Models/MessageModel");
var UserModel = require("../Models/UserModel");
var Settings = require("../lib/Settings");
const App4OfferService = require('../lib/App4OfferService');

var DeleteMessageActionHandler = function(){
	
}

_.extend(DeleteMessageActionHandler.prototype,SocketHandlerBase.prototype);

DeleteMessageActionHandler.prototype.attach = function(io,socket){
		
	var self = this;

	/**
	 * @api {socket} "deleteMessage" Delete Message
	 * @apiName Delete Message
	 * @apiGroup Socket 
	 * @apiDescription Delete Message
	 * @apiParam {string} userID User ID
	 * @apiParam {string} messageID Message ID
	 *
	 */
	 
	socket.on('banUser', async function(param){

		if (Utils.isEmpty(param.userID)) {
			socket.emit('socketerror', {code: Const.resCodeSocketDeleteMessageNoUserID});
			return;
        }

		let isAdmin = await App4OfferService.getUserIsAdmin(param.userID);
        if (!isAdmin) {
            return;
		}

		let moderatorUser = await UserModel.findUserbyId(param.userID);

        let message = await MessageModel.findMessagebyId(param.messageID);
        let userIsAdmin = await App4OfferService.getUserIsAdmin(message.userID);
        if (userIsAdmin) {
            return;
        }

		try {
            let socketId = UsersManager.getSocketByUserID(message.userID);
			let user = await UserModel.findUserbyId(message.userID);
            await user.ban();
            io.of(Settings.options.socketNameSpace).to(socketId).emit('socketerror', {
                code: Const.resCodeSocketUserBanned,
                till: user.bannedTill - Utils.now()
			});
			
			let adminUser = await UserModel.model.findOne({userID: 'admin'}).exec();
			SocketAPIHandler.io.of(Settings.options.socketNameSpace).in(message.roomID).emit('userBan', {
				_id: 'temp',
				user: adminUser.toObject(),
				userID: adminUser.userID,
				roomID: param.roomID,
				message: `Пользователь ${user.name} забанен модератором ${moderatorUser.name}`,
				type: 1002,
				file: null,
				appID: adminUser.appID,
				created: Utils.now() - 50,
				viewed: true
			});
            return;
		} catch (err) {
			if (err) {
				socket.emit('socketerror', {code: Const.resCodeSocketUnknownError});
				return;
			}
		}
	});
}


module["exports"] = new DeleteMessageActionHandler();