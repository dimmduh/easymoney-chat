var _ = require('lodash');

var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");
var SocketHandlerBase = require("./SocketHandlerBase");
var SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');
const PushNotifiactionService = require('../lib/PushNotificationService');
const UserModel = require('../Models/UserModel');
const App4OfferService = require('../lib/App4OfferService');
const MessageModel = require('../Models/MessageModel');

var SendMessageActionHandler = function(){
	
}

var SendMessageLogic = require('../Logics/SendMessage');

var BridgeManager = require('../lib/BridgeManager');

_.extend(SendMessageActionHandler.prototype,SocketHandlerBase.prototype);

SendMessageActionHandler.prototype.attach = function(io,socket){
	
	var self = this;

	/**
	 * @api {socket} "sendMessage" Send New Message
	 * @apiName Send Message
	 * @apiGroup Socket 
	 * @apiDescription Send new message by socket
	 * @apiParam {string} roomID Room ID
	 * @apiParam {string} userID User ID
	 * @apiParam {string} type Message Type. 1:Text 2:File 3:Location
	 * @apiParam {string} message Message if type == 1
	 * @apiParam {string} fileID File ID if type == 2
	 * @apiParam {object} location lat and lng if type == 3
	 *
	 */
	 
	socket.on('sendMessage', function(param){
		
		if (Utils.isEmpty(param.roomID)) {
			socket.emit('socketerror', {code:Const.resCodeSocketSendMessageNoRoomID});
			return;
		}
		
		if (Utils.isEmpty(param.userID)) {
			socket.emit('socketerror', {code:Const.resCodeSocketSendMessageNoUserId});
			return;
		}
		
		if (Utils.isEmpty(param.type)) {
			socket.emit('socketerror', {code:Const.resCodeSocketSendMessageNoType});
			return;
		}
		
		if (param.type == Const.messageTypeText && Utils.isEmpty(param.message)) {
			socket.emit('socketerror', {code:Const.resCodeSocketSendMessageNoMessage});
			return;
		}
		
		BridgeManager.hook('sendMessage', param, function(result){
			if (result == null || result.canSend) {
				SendMessageLogic.execute(param.userID, param, async function(result){
					if (param.direct && UsersManager.getUsers(param.roomID).length < 2) {
						let clientTo = await UserModel.findUserbyId(param.clientToken);
						if (clientTo !== null) {
							PushNotifiactionService.directMessage(await UserModel.findUserbyId(param.clientToken), 
								await UserModel.findUserbyId(param.userID), 
								param.roomID, 
								await App4OfferService.canGetPush(param.clientToken));
						}
					}

					if (!Utils.isEmpty(param.messageID)) {
						let message = await MessageModel.findMessagebyId(param.messageID);
						if (message.userID != param.userID) {
							PushNotifiactionService.reply(await UserModel.findUserbyId(message.userID), result, await App4OfferService.canGetPush(message.userID));
						}
					}

				}, function(err, code, data){
					if (err) {
						switch(err.message) {
							case 'User banned' : socket.emit('socketerror', {code:Const.resCodeSocketUserBanned, till: data.till});
								break;
							case 'Not enough money' : socket.emit('socketerror', {code:Const.resCodeSocketNotEnoughMoney});
								break;
							default : socket.emit('socketerror', {code:code});
								break;
						}
					} else {
						socket.emit('socketerror', {code:code});
					}
				});
			}
		});
	});
}

module["exports"] = new SendMessageActionHandler();