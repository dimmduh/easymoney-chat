var _ = require('lodash');
var Observer = require("node-observer");

var Utils = require("../lib/Utils");
var Const = require("../const");
var SocketHandlerBase = require("./SocketHandlerBase");
var MessageModel = require("../Models/MessageModel");
const RoomModel = require('../Models/RoomModel');
var Settings = require("../lib/Settings");
const App4OfferService = require('../lib/App4OfferService');

var DeleteMessageActionHandler = function(){
	
}

_.extend(DeleteMessageActionHandler.prototype,SocketHandlerBase.prototype);

DeleteMessageActionHandler.prototype.attach = function(io,socket){
		
	var self = this;

	/**
	 * @api {socket} "deleteMessage" Delete Message
	 * @apiName Delete Message
	 * @apiGroup Socket 
	 * @apiDescription Delete Message
	 * @apiParam {string} userID User ID
	 * @apiParam {string} messageID Message ID
	 *
	 */
	 
	socket.on('deleteMessage', async function(param){
		
		if(Utils.isEmpty(param.userID)){
			socket.emit('socketerror', {code:Const.resCodeSocketDeleteMessageNoUserID});
			return;
		}

		let isAdmin = await App4OfferService.getUserIsAdmin(param.userID);

		if (!isAdmin) {
			return;
		}

		if(Utils.isEmpty(param.messageID)){
			socket.emit('socketerror', {code:Const.resCodeSocketDeleteMessageNoMessageID});
			return;
		}
		
		let message = await MessageModel.findMessagebyId(param.messageID);
		let isAdminMessage = await App4OfferService.getUserIsAdmin(message.userID);
		let isPaidRoom = await RoomModel.isPaidRoom(message.roomID);

		if (isAdminMessage || isPaidRoom) {
			return;
		}

		message.update({
			message: '',
			file: null,
			location:null,
			deleted: Utils.now()
		}, {}, function(err, userResult){
			MessageModel.populateMessages(message, param.userID, function (err,messages) {
				if (err) {
					socket.emit('socketerror', {code:Const.resCodeSocketUnknownError});
					return;
				}
				
				if (messages.length > 0) {
					var obj = messages[0];
					obj.deleted = Utils.now();
					obj.message = '';
					obj.file = null;
					obj.location = null;

					// send updated messages
					io.of(Settings.options.socketNameSpace).in(messages[0].roomID).emit('messageUpdated', [obj]);
					Observer.send(this, Const.notificationMessageChanges, [obj]);
				}
			});
		});
		
	});

}


module["exports"] = new DeleteMessageActionHandler();