var _ = require('lodash');
var Observer = require("node-observer");

var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");
var SocketHandlerBase = require("./SocketHandlerBase");
var UserModel = require("../Models/UserModel");
const MessageModel = require('../Models/MessageModel');
var Settings = require("../lib/Settings");

var LoginActionHandler = function(){
	
}

_.extend(LoginActionHandler.prototype,SocketHandlerBase.prototype);

LoginActionHandler.prototype.attach = function(io,socket){
	
	var self = this;

	/**
	 * @api {socket} "login" Login to the room
	 * @apiName Login to room
	 * @apiGroup Socket 
	 * @apiDescription Login to room
	 * @apiParam {string} roomID Room ID
	 *
	 */
	socket.on('login', async function(param) {
		if (Utils.isEmpty(param.userID)) {
			socket.emit('socketerror', {code: Const.resCodeSocketLoginNoUserID});
			return;
		}

		if (Utils.isEmpty(param.roomID)) {
			socket.emit('socketerror', {code: Const.resCodeSocketLoginNoRoomID});
			return;
		}
		
		socket.join(param.roomID);
		io.of(Settings.options.socketNameSpace).in(param.roomID).emit('newUser', param);
		Observer.send(this, Const.notificationNewUser, param);

		//save as message
		let user = await UserModel.findUserbyId(param.userID);

		if (user === null) {
			socket.emit('socketerror', {code: Const.resCodeSocketUnknownError});
			return;
		}

		if (!Utils.isEmpty(user.bannedTill) && (!param.direct)) {
			if (param.roomID.search('support') < 0) {
				if (user.bannedTill == 0) {
					socket.emit('socketerror', {
						code: Const.resCodeSocketUserBanned,
						till: -1
					});
					return;
				}
	
				if (user.bannedTill > Utils.now()) {
					socket.emit('socketerror', {
						code: Const.resCodeSocketUserBanned,
						till: user.bannedTill - Utils.now()
					});
					return;
				}
			}
		}

		UsersManager.addUser(param.userID, user.name, user.avatarURL, param.roomID, user.token);
		UsersManager.pairSocketIDandUserID(param.userID, socket.id);
		MessageModel.setViewed(param.userID, param.roomID);
	});

}

module["exports"] = new LoginActionHandler();