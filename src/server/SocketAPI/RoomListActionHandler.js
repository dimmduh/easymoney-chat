var _ = require('lodash');
var Observer = require('node-observer');

var UsersManager = require('../lib/UsersManager');
var DatabaseManager = require('../lib/DatabaseManager');
var Utils = require('../lib/Utils');
var Const = require('../const');
var SocketHandlerBase = require("./SocketHandlerBase");
var UserModel = require("../Models/UserModel");
var Settings = require("../lib/Settings");
var MessageModel = require('../Models/MessageModel');

var RoomListHandler = function() {

}

_.extend(RoomListHandler.prototype, SocketHandlerBase.prototype);

RoomListHandler.prototype.attach = function(io, socket) {

    var self = this;

    socket.on('roomList', function(param) {
        MessageModel.getRoomList();
    });

}

module['exports'] = new RoomListHandler();