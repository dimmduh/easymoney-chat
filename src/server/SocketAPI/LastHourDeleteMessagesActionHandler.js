var _ = require('lodash');
var Observer = require("node-observer");

var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");
var SocketHandlerBase = require("./SocketHandlerBase");
var SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');
var MessageModel = require("../Models/MessageModel");
var UserModel = require("../Models/UserModel");
var Settings = require("../lib/Settings");
const App4OfferService = require('../lib/App4OfferService');

var DeleteMessageActionHandler = function(){
	
}

_.extend(DeleteMessageActionHandler.prototype,SocketHandlerBase.prototype);

DeleteMessageActionHandler.prototype.attach = function(io,socket){
		
	var self = this;

	/**
	 * @api {socket} "deleteMessage" Delete Message
	 * @apiName Delete Message
	 * @apiGroup Socket 
	 * @apiDescription Delete Message
	 * @apiParam {string} userID User ID
	 * @apiParam {string} messageID Message ID
	 *
	 */
	 
	socket.on('deleteLastHourMessages', async function(param){
		if (Utils.isEmpty(param.userID)) {
			socket.emit('socketerror', {code: Const.resCodeSocketDeleteMessageNoUserID});
			return;
        }

        let isAdmin = await App4OfferService.getUserIsAdmin(param.userID);
        if (!isAdmin) {
            return;
        }

        let message = await MessageModel.findMessagebyId(param.messageID);
        let userIsAdmin = await App4OfferService.getUserIsAdmin(message.userID);
        if (userIsAdmin) {
            return;
        }

		try {
			await MessageModel.deleteByUserIDInLastHour(message.userID, message.roomID);
			let messages = await MessageModel.findMessagesInLastHour(message.userID, message.roomID);
			MessageModel.populateMessages(messages, param.userID, function(err, messages) {
				if (err) {
					socket.emit('socketerror', {code:Const.resCodeSocketUnknownError});
					return;
				}

				if (messages.length > 0) {
					// send updated messages
					io.of(Settings.options.socketNameSpace).in(messages[0].roomID).emit('messageUpdated', messages);
					Observer.send(this, Const.notificationMessageChanges, messages);
				}
			});
		} catch (err) {
			if (err) {
				socket.emit('socketerror', {code: Const.resCodeSocketUnknownError});
				return;
			}
		}
	});
}


module["exports"] = new DeleteMessageActionHandler();