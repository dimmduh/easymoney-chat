var _ = require('lodash');
var Observer = require("node-observer");

var UsersManager = require("../lib/UsersManager");
var DatabaseManager = require("../lib/DatabaseManager");
var Utils = require("../lib/Utils");
var Const = require("../const");
var SocketHandlerBase = require("./SocketHandlerBase");
var SocketAPIHandler = require('../SocketAPI/SocketAPIHandler');
var MessageModel = require("../Models/MessageModel");
var UserModel = require("../Models/UserModel");
var Settings = require("../lib/Settings");
const App4OfferService = require('../lib/App4OfferService');

var DeleteMessageActionHandler = function(){
	
}

_.extend(DeleteMessageActionHandler.prototype,SocketHandlerBase.prototype);

DeleteMessageActionHandler.prototype.attach = function(io,socket){
		
	var self = this;

	/**
	 * @api {socket} "deleteMessage" Delete Message
	 * @apiName Delete Message
	 * @apiGroup Socket 
	 * @apiDescription Delete Message
	 * @apiParam {string} userID User ID
	 * @apiParam {string} messageID Message ID
	 *
	 */
	 
	socket.on('makeComplaint', async function(param){
		
		if (Utils.isEmpty(param.userID)) {
			socket.emit('socketerror', {code: Const.resCodeSocketDeleteMessageNoUserID});
			return;
		}

		if (Utils.isEmpty(param.messageID)) {
			socket.emit('socketerror', {code: Const.resCodeSocketDeleteMessageNoMessageID});
			return;
		}

		let message = await MessageModel.findMessagebyId(param.messageID);

		var fields = {
			complaints: message.complaints,
			file: message.file,
			message: message.message,
		};

		let isAdmin = await App4OfferService.getUserIsAdmin(param.userID);

		if (_.indexOf(fields.complaints, param.userID) == -1) {
			fields.complaints.push(param.userID);
		}

		if ((fields.complaints.length >= 10) || isAdmin) {
			fields.file = null;
			fields.message = '';
			fields.deleted = Utils.now();
			fields.type = 1;
		}

		if (await App4OfferService.getUserIsAdmin(message.userID)) {
			return;
		}

		try {
			await message.update(fields).exec();

			if (isAdmin) {
				let socketId = UsersManager.getSocketByUserID(message.userID);
				let user = await UserModel.findUserbyId(message.userID);
				await user.ban();
				io.of(Settings.options.socketNameSpace).to(socketId).emit('socketerror', {
					code: Const.resCodeSocketUserBanned,
					till: user.bannedTill - Utils.now()
				});
				MessageModel.deleteByUserID(message.userID, message.roomID);
			}

			MessageModel.populateMessages(message, 0, function(err, messages) {
				if (err) {
					socket.emit('socketerror', {code: Const.resCodeSocketUnknownError});
					return;
				}

				if (messages.length > 0) {
					if (messages[0].deleted != undefined) {
						messages[0].deleted = Utils.now();
						io.of(Settings.options.socketNameSpace).in(messages[0].roomID).emit('messageUpdated', [messages[0]]);
						Observer.send(this, Const.notificationMessageChanges, [messages[0]]);
					}
				}
			});
		} catch (err) {
			if (err) {
				socket.emit('socketerror', {code: Const.resCodeSocketUnknownError});
				return;
			}
		}
	});
}


module["exports"] = new DeleteMessageActionHandler();