const DatabaseManager = require('./DatabaseManager');
const axios = require('axios');
const Utils = require('./Utils');

const baseUrl = 'http://app4offer.com/chat/';

async function sendRequest(resource, userID) {
	return axios.get(baseUrl + resource + `?token=${userID}`);
}

const request = {
	pay: async function(userID, cost) {
		try {
			let response = await sendRequest('pay', `${userID}&cost=${cost}`);
			return response.status == 201;
		} catch (err) {
			return false;
		}
	},
	canPostPhoto: async function(userID) {
		try {
			let response = await sendRequest('can-post-photo', userID);
			return response.status == 200;
		} catch (err) {
			return false;
		}
	},
	getRegistrationToken: async function(userID) {
		try {
			let response = await sendRequest('get-registration-token', userID);
			return response.data;
		} catch (err) {
			return false;
		}
	},
	canGetPush: async function(userID) {
		try {
			let response = await sendRequest('can-get-push-notification', userID);
			return response.data;
		} catch (err) {
			return false;
		}
	},
	getAppID: async function(userID) {
		try {
			let response = await sendRequest('get-app-id', userID);
			return response.data;
		} catch (err) {
			return false;
		}
	},
	getLocale: async function(userID) {
		try {
			let response = await sendRequest('get-locale-and-app-name', userID);
			return response.data;
		} catch (err) {
			return false;
		}
	},
	getUserIsAdmin: async function(userID) {
		try {
			let response = await sendRequest('get-user-is-admin', userID);
			return !!response.data;
		} catch (err) {
			return false;
		}
	}
}

const App4OfferService = {
	payForMessage: async function(userID, cost) {
		if (Utils.isEmpty(cost) || (cost == 0)) {
			return true;
		}

		return await request.pay(userID, cost);
	},

	canPostPhoto: async function(userID, isPaid) {
		if (isPaid) {
			return true;
		}

		return await request.canPostPhoto(userID);
	},

	getRegistrationToken: async function(userID) {
		let data = await request.getRegistrationToken(userID);
		return data['registration_token'];
	},

	canGetPush: async function(userID) {
		let data = await request.canGetPush(userID);
		return data;
	},

	getAppID: async function(userID) {
		return await request.getAppID(userID);
	},

	getLocale: async function(userID) {
		return await request.getLocale(userID);
	},

	getUserIsAdmin: async function(userID) {
		return await request.getUserIsAdmin(userID);
	}
}

module.exports = App4OfferService;