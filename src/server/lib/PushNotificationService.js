const queueName = 'a4o-push-notification';
const types = {
    directMessage : 'direct_message',
    bannedFromChat : 'banned_from_chat',
    chatReply : 'chat_reply'
};
const open = require('amqplib').connect('amqp://localhost:5672');
const Utils = require('./Utils');
const App4OfferService = require('./App4OfferService');
let i18n = require('i18n');

i18n.configure({
    'locales': ['ru-RU', 'en-US'],
    'directory': __dirname + '/../locales'
});

const PushNotificationService = {

    ch : null,
    conn : null,

    initChannel : async function() {
        if (this.conn === null) {
            this.conn = await open;
        }
        if (this.ch === null) {
            this.ch = await this.conn.createChannel();
            let ok = this.ch.assertQueue(queueName);
        }
    },

    directMessage : async function(clientTo, clientFrom, roomID, hasBody) {
        await this.initChannel();
        let localeData = await App4OfferService.getLocale(clientTo.userID)
        i18n.setLocale(localeData.locale);
        if (!Utils.isEmpty(clientTo.registrationToken)) {
            await this.ch.sendToQueue(queueName, new Buffer(JSON.stringify({
                token : clientTo.registrationToken,
                type : types.directMessage,
                title : i18n.__mf('{name} send you a message', {name : clientFrom.name}),
                body : localeData.appName,
                payload : {
                    public_token: clientFrom.userID,
                    name: clientFrom.name,
                    room_id: roomID
                },
                hasBody: hasBody
            })));
        }
    },

    ban : async function(client, period) {
        await this.initChannel();
        let localeData = await App4OfferService.getLocale(client.userID)
        i18n.setLocale(localeData.locale);
        if (!Utils.isEmpty(client.registrationToken)) {
            await this.ch.sendToQueue(queueName, new Buffer(JSON.stringify({
                token : client.registrationToken,
                type : types.bannedFromChat,
                title : i18n.__mf(`Now you are banned from chat ${period}`),
                body : localeData.appName,
                payload : {},
                hasBody : true
            })));
        }
    },

    reply : async function(client, message, hasBody) {
        await this.initChannel();
        let localeData = await App4OfferService.getLocale(client.userID);
        i18n.setLocale(localeData.locale);
        if (!Utils.isEmpty(client.registrationToken)) {
            await this.ch.sendToQueue(queueName, new Buffer(JSON.stringify({
                token : client.registrationToken,
                type : types.chatReply,
                title : i18n.__mf('You have a reply on your chat message'),
                body : localeData.appName,
                payload : {
                    message : message._id,
                    room_id : message.roomID
                },
                hasBody : hasBody
            })));
        }
    }

};

module.exports = PushNotificationService;